set nocompatible

execute pathogen#infect()
syntax on
filetype plugin indent on

set laststatus=2

" Настройка GUI
if has("gui_running")
    set guioptions-=m
    set guioptions-=T
    set guioptions-=r
    set guioptions-=L
    set guifont=Monospace\ 13
    " let g:airline#extensions#tabline#enabled=1
endif

color harlequin

set list
set listchars=tab:>-,trail:-

" Поиск
set incsearch
set hlsearch

set noswapfile

set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set smartindent

set wrap
set scrolloff=3
set linebreak
set cursorline
set colorcolumn=80


set number

" Plugins
" NERDTree
let NERDTreeIgnore = ['\.pyc$']

" Map
nnoremap <Esc> :noh<return><Esc>

" map <C-Tab> :bn<CR>
" imap <C-Tab> <Esc>:bn<CR>
" vmap <C-Tab> <Esc>:bn<CR>

" map <C-S-Tab> :bp<CR>
" imap <C-S-Tab> <Esc>:bp<CR>
" vmap <C-S-Tab> <Esc>:bp<CR>

map <F4> :bd<CR>
imap <F4> <Esc>:bd<CR>
vmap <F4> <Esc>:bd<CR>

silent! nmap <C-p> :NERDTreeToggle<CR>
silent! nmap <C-l> :BufExplorer<CR>

" map <F2> :BufExplorer<CR>
" imap <F2> <Esc>:BufExplorer<CR>
" vmap <F2> <Esc>:BufExplorer<CR>
